package autotests.tests;

import autotests.clients.DuckPropertiesClient;
import autotests.payloads.DuckProperties;
import com.consol.citrus.TestActionRunner;
import com.consol.citrus.TestCaseRunner;
import com.consol.citrus.annotations.CitrusResource;
import com.consol.citrus.annotations.CitrusTest;
import org.springframework.context.annotation.Description;
import org.springframework.http.HttpStatus;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

public class getDuckPropertiesTest extends DuckPropertiesClient {

    @Description("Получить характеристики уточки")
    @CitrusTest
    @Test
    public void successGetProperties(@Optional @CitrusResource TestCaseRunner runner) {
        DuckProperties duckProperties = new DuckProperties().material("rubber");

        getDuckProperties(runner, duckProperties);
        validateResponse(runner, HttpStatus.OK, "getDuckPropertiesTest/successGetProperties.json");
    }
}
