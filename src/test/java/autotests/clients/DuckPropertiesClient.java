package autotests.clients;


import autotests.BaseTest;
import com.consol.citrus.TestActionRunner;
import com.consol.citrus.TestCaseRunner;
import com.consol.citrus.http.client.HttpClient;
import org.springframework.context.annotation.Description;
import org.springframework.http.HttpStatus;

public class DuckPropertiesClient extends BaseTest {

    @Description("Получить характеристики уточки")
    public void getDuckProperties(TestCaseRunner runner, Object payload) {
        sendPostRequest(runner, yellowDuck, "/api/duck/properties", payload);
    }

    @Description("Валидация ответа")
    public void validateResponse(TestCaseRunner runner, HttpStatus status, String expectedPayload){
        validateResponse(runner, yellowDuck, status, expectedPayload);
    }
}

